#门户网站api文档

`/api/resource_info/[id]`
返回对应id的资源的浏览次数和喜欢次数的信息
```
{
    'id':'1', 
    'browsed':'1', 
    'liked':'1'
}
```
<br/>

`/api/resource_content/[id]`
返回对应id的资源的正文内容及相关信息

```
{
    'id':'1', 
    'title':'XXX数据',
    'content':'这个是XXX的数据',
    'author':'lixiaoying'
    'download':'http://www.lierenio.com/api/download/XXX.csv',
    'time':'2017-04-23',
    'tags':['互联网+','金融']

}
```
<br/>

`/api/complain/[id]`
用于资源正文页的举报按钮，举报某个资源，举报后返回相关的成功与否的提示

`/api/download/[XXX.XXX]`
用于资源正文页的下载按钮，资源下载链接，参数为空返回错误提示

`/api/customize`
用于个性定制页面表单的提交按钮,点击后返回相关提示

`__STATIC__/[module]/[css\js\image]/filename`
如：
`__STATIC__/index/css/index.css`
`__STATIC__/index/image/1.jpg`
用来存放静态资源

`/account/login`
登陆页面，同时也是处理登陆逻辑的api

`account/login/logout `
处理退出登陆的api

`/account/register`
注册页面

`account/register/register`
处理注册逻辑的api

`/account/user'
用户中心主页面

`api/report/get_all_report`
获取所有报告的信息，比如：
```
[{"report_id":1,"title":"\u54c8\u54c8","content":"\u54c8\u54c8\u54c8\u54c8\u54c8","category":"quwen","upload_date":"2015-5-15","browsed":1},

{"report_id":2,"title":"\u8bba\u8272\u60c5\u884c\u4e1a\u7684\u53d1\u5c55","content":"\u5927\u6709\u53ef\u4e3a\uff01\uff01\uff01","category":"qingbao","upload_date":"2015-5-15","browsed":1},

{"report_id":3,"title":"\u5475\u5475","content":"\u674e\u6653\u83b9\u662f\u5927\u7f8e\uff08sha\uff09\u5973\uff08bi\uff09\uff01\uff01","category":"quwen","upload_date":"2015-5-15","browsed":1},

{"report_id":4,"title":"\u8bb2\u7b11\u8bdd","content":"\u563b\u563b\u54c8\u54c8","category":"quwen","upload_date":"2017-05-15","browsed":0}]
```
<br/>

`api/report/get_one_report/:report_id`
获取一个报告,如：
```
{"report_id":1,"title":"\u54c8\u54c8","content":"\u54c8\u54c8\u54c8\u54c8\u54c8","category":"quwen","upload_date":"2015-5-15","browsed":1}
```
<br/>

`api/report/get_some_report/:category`
获取一个类别的报告，如：`api/report/get_some_report/quwen`
```
[{"report_id":1,"title":"\u54c8\u54c8","content":"\u54c8\u54c8\u54c8\u54c8\u54c8","category":"quwen","upload_date":"2015-5-15","browsed":1},

{"report_id":3,"title":"\u5475\u5475","content":"\u674e\u6653\u83b9\u662f\u5927\u7f8e\uff08sha\uff09\u5973\uff08bi\uff09\uff01\uff01","category":"quwen","upload_date":"2015-5-15","browsed":1},

{"report_id":4,"title":"\u8bb2\u7b11\u8bdd","content":"\u563b\u563b\u54c8\u54c8","category":"quwen","upload_date":"2017-05-15","browsed":0}]
```
<br/>

`api/report/upload_report`
上传快报，给管理人员使用，如：
```
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
    data={"title":"aaa", "content":"bbbbbb", "category":"quwen"}
    $.post('http://www.dataforest.com/api/report/upload_report', data);
</script>
```
<br/>

`api/report/complain`
用于举报一个快报
```
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
    data={"report_id": "2", "reasons[]":["谣言", "低俗"]};
    $.post('http://www.dataforest.com/api/report/complain', data);
</script>
```
<br/>

`api/resouce/complain`
用于举报一个资源，用法同上