-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2017-05-16 10:52:51
-- 服务器版本： 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dataforest`
--

-- --------------------------------------------------------

--
-- 表的结构 `customize_info`
--

CREATE TABLE IF NOT EXISTS `customize_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `submit_date` varchar(20) DEFAULT NULL,
  `planned_date` varchar(20) DEFAULT NULL,
  `type` tinyint(3) unsigned DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `format` tinyint(3) unsigned DEFAULT NULL,
  `num_of_records` tinyint(3) unsigned DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `customize_info`
--

INSERT INTO `customize_info` (`id`, `name`, `email`, `phone`, `submit_date`, `planned_date`, `type`, `url`, `format`, `num_of_records`, `description`) VALUES
(1, 'qige', 'zrq96@163.com', '13697452365', '2017-04-27', '2017-7-1', 0, 'www.baidu.com', 0, 0, '话说那个直呼类型和url是什么鬼？'),
(2, 'qige', 'zrq96@163.com', '13697452365', '2017-04-27', '2017-7-1', 0, 'www.baidu.com', 0, 0, '话说那个直呼类型和url是什么鬼？'),
(3, 'ray', 'ray@163.com', '123', '2017-04-27', '123', 0, '132', 0, 0, 'aaaaaaaaaaa'),
(4, '陈钟南', '990112156@qq.com', '111111111', '2017-04-28', '2017-5-1', 0, 'www.baidu.com', 5, 1, '爱你么么哒'),
(5, '', '', '', '2017-05-03', '', NULL, '', NULL, NULL, '');

-- --------------------------------------------------------

--
-- 表的结构 `report`
--

CREATE TABLE IF NOT EXISTS `report` (
  `report_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(80) NOT NULL,
  `content` text NOT NULL,
  `category` char(10) NOT NULL,
  `upload_date` varchar(20) NOT NULL,
  `browsed` int(10) unsigned NOT NULL,
  PRIMARY KEY (`report_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `report`
--

INSERT INTO `report` (`report_id`, `title`, `content`, `category`, `upload_date`, `browsed`) VALUES
(1, '哈哈', '哈哈哈哈哈', 'quwen', '2015-5-15', 1),
(2, '论色情行业的发展', '大有可为！！！', 'qingbao', '2015-5-15', 1),
(3, '呵呵', '李晓莹是大美（sha）女（bi）！！', 'quwen', '2015-5-15', 1),
(4, '讲笑话', '嘻嘻哈哈', 'quwen', '2017-05-15', 0);

-- --------------------------------------------------------

--
-- 表的结构 `report_complaint`
--

CREATE TABLE IF NOT EXISTS `report_complaint` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `report_id` int(11) NOT NULL,
  `complaint_reason` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- 转存表中的数据 `report_complaint`
--

INSERT INTO `report_complaint` (`id`, `report_id`, `complaint_reason`) VALUES
(1, 1, '重复、旧闻'),
(2, 1, '标题党'),
(3, 1, '标题党'),
(4, 1, '疑似抄袭'),
(5, 1, '疑似抄袭'),
(6, 1, '重复、旧闻'),
(7, 1, '低俗'),
(8, 1, '标题党'),
(9, 1, '谣言'),
(10, 1, '谣言'),
(11, 1, '广告'),
(12, 1, '内容质量差'),
(13, 1, '疑似抄袭'),
(14, 1, '谣言'),
(15, 1, '谣言'),
(16, 1, '谣言'),
(17, 1, '谣言'),
(18, 1, '质量差'),
(19, 1, '标题党'),
(20, 1, '谣言');

-- --------------------------------------------------------

--
-- 表的结构 `resource`
--

CREATE TABLE IF NOT EXISTS `resource` (
  `resource_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  `author` varchar(50) DEFAULT NULL,
  `dateline` int(11) DEFAULT NULL,
  `category` tinyint(3) unsigned DEFAULT NULL,
  `isfree` tinyint(3) unsigned DEFAULT NULL,
  `browsed` int(10) unsigned DEFAULT NULL,
  `liked` int(10) unsigned DEFAULT NULL,
  `download_link` varchar(300) DEFAULT NULL,
  `tag` varchar(100) NOT NULL,
  PRIMARY KEY (`resource_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- 转存表中的数据 `resource`
--

INSERT INTO `resource` (`resource_id`, `title`, `content`, `author`, `dateline`, `category`, `isfree`, `browsed`, `liked`, `download_link`, `tag`) VALUES
(1, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\3fbda77883ac93e25fec3e35583689cd.gephi_temp1475729950746', '测试'),
(2, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\933d4c5fc3ea8b8c3ca294969a88d701.gephi_temp1475729950746', '测试'),
(3, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\e31ab281fe96acfb7299a1e71825c3a5.gephi_temp1475729950746', '测试'),
(4, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\f718b75d6332f7200bd61d6fd91687ad.gephi_temp1475729950746', '测试'),
(5, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\4fe5d939e262448e301b0ce7146b8188.gephi_temp1475729950746', '测试'),
(6, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\e41a0bb4c8e95d7db05394cac0659bfc.gephi_temp1475729950746', '测试'),
(7, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\f10e9eb60c9eaacf7beac47148aa5c15.gephi_temp1475729950746', '测试'),
(8, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\b8c6cf385810330a5c864a06feb796a1.gephi_temp1475729950746', '测试'),
(9, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\0be70e4f3e71ed9638bd45815521464f.gephi_temp1475729950746', '测试'),
(10, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\e4f31f0b36b6684b3b133e3597d32c3b.gephi_temp1475729950746', '测试'),
(11, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\aafb9972e8d72ce1dea1761a2c07c0af.gephi_temp1475729950746', '测试'),
(12, '11', '111', '11', 1493313600, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170428\\e632045e5f6d2658c9c7e0e54614279d.gephi_temp1475729950746', '测试2'),
(13, '11', '111', '11', 1493313685, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170428\\756ba21312f28e4101776fac59ecf873.gephi_temp1475729950746', '测试2'),
(14, '1', '1', '1', 1493313703, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170428\\e612e7cf79a934806431ea5d43cc03fc.gephi_temp1475729950746', '1'),
(15, '11', '11', '11', 1493313833, 11, 11, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170428\\ca40a8daef229ff8301a7168577edeae.gephi_temp1475729950746', '1'),
(16, 'pdf', '信管专业教学方案', 'qige', 1493818202, 2, 1, NULL, NULL, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170503\\eb2202d805f285b465583815c010669b.pdf', '教学'),
(17, '信管培养方案', '这是一个信管专业的培养方案pdf', '七个', 1493975699, 0, 0, NULL, NULL, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170505\\cab4084d3c1540425d4e0311cbc967fa.pdf', '信管'),
(18, '瑞士交易所', '啊哈佛啊神佛啊是of环境', '发UI双方', 1494571787, 1, 1, NULL, NULL, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170512\\c5f3e9b04839a3c765b0665e46213145.xls', '1');

-- --------------------------------------------------------

--
-- 表的结构 `resource_complaint`
--

CREATE TABLE IF NOT EXISTS `resource_complaint` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `resource_id` bigint(20) unsigned DEFAULT NULL,
  `complaint_reason` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- 转存表中的数据 `resource_complaint`
--

INSERT INTO `resource_complaint` (`id`, `resource_id`, `complaint_reason`) VALUES
(1, 1, '内容质量差'),
(2, 1, '标题党'),
(3, 1, '疑似抄袭'),
(4, 1, '标题党'),
(5, 1, '疑似抄袭'),
(6, 1, '疑似抄袭'),
(7, 1, '疑似抄袭'),
(8, 1, '疑似抄袭'),
(9, 1, '重复、旧闻'),
(10, 1, '重复、旧闻'),
(11, 1, '重复、旧闻'),
(12, 1, '低俗'),
(13, 1, '标题党'),
(14, 1, '谣言'),
(15, 1, '广告'),
(16, 1, '内容质量差'),
(17, 1, '疑似抄袭');

-- --------------------------------------------------------

--
-- 表的结构 `resource_tag`
--

CREATE TABLE IF NOT EXISTS `resource_tag` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `resource_id` bigint(20) unsigned DEFAULT NULL,
  `tag` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- 转存表中的数据 `resource_tag`
--

INSERT INTO `resource_tag` (`id`, `resource_id`, `tag`) VALUES
(1, 11, '测试'),
(2, 12, '测试2'),
(3, 13, '测试2'),
(4, 14, '1'),
(5, 15, '1'),
(6, 16, '教学'),
(7, 17, '信管'),
(8, 18, '1');

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `nick_name` varchar(16) NOT NULL,
  `organization` varchar(30) NOT NULL,
  `specialization` varchar(30) NOT NULL,
  `signature` varchar(100) NOT NULL,
  `create_date` varchar(20) DEFAULT NULL,
  `access_token` varchar(32) CHARACTER SET utf16 NOT NULL,
  `expire_time` varchar(20) NOT NULL,
  `activated` tinyint(4) NOT NULL DEFAULT '0',
  `level` tinyint(3) unsigned DEFAULT NULL,
  `profile_link` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`user_id`, `username`, `password`, `nick_name`, `organization`, `specialization`, `signature`, `create_date`, `access_token`, `expire_time`, `activated`, `level`, `profile_link`) VALUES
(13, 'rickyzhu@foxmail.com', '0437a4ba8936e22f8440c078d2eef253', '', '', '', '', '2017-05-15 00:23:56', 'b6378fbb2e38206de638e26cc6e68ea0', '1494779036', 1, 1, 'D:\\softwares\\wamp\\www\\dataforest\\public\\static\\img'),
(14, '936932302@qq.com', '0437a4ba8936e22f8440c078d2eef253', '', '', '', '', '2017-05-15 15:42:00', '6671eed2828ca745238bd61fec616b96', '1494834120', 1, 1, 'D:\\softwares\\wamp\\www\\dataforest\\public\\static\\img'),
(15, '1433192948@qq.com', '5d77f85e735e40299f482ae664511ba9', '', '', '', '', '2017-05-15 15:57:12', '93e9335a51a5e5c2b89786f444842236', '1494835032', 0, 1, 'D:\\softwares\\wamp\\www\\dataforest\\public\\static\\img');

-- --------------------------------------------------------

--
-- 表的结构 `user_tag`
--

CREATE TABLE IF NOT EXISTS `user_tag` (
  `user_id` int(10) unsigned NOT NULL,
  `tag` varchar(16) NOT NULL,
  PRIMARY KEY (`user_id`,`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
