/*
第一版门户网站用到的数据表
*/

-- 创建资源表
create table if not exists resource(
	resource_id bigint unsigned key auto_increment,
	title varchar(100),
	content text,
	author varchar(50),
	dateline int(11),
	category tinyint unsigned,
	isfree tinyint unsigned,
	browsed int unsigned,
	liked int unsigned,
	download_link varchar(100)
);

-- 创建资源标签表
create table  if not exists resource_tag(
	id bigint unsigned key auto_increment,
	resource_id bigint unsigned,
	tag varchar(20)
);

-- 创建举报信息表
create table  if not exists complaint(
	id bigint unsigned key auto_increment,
	resource_id bigint unsigned,
	complaint_reason varchar(100)
);


-- 创建用户表（暂时是这样吧，以后再改）
create table  if not exists user(
	user_id bigint key auto_increment,
	user_name varchar(20),
	password varchar(32),
	create_date varchar(20),
	level tinyint unsigned,
	profile_link varchar(50)
);

-- 创建个性定制的订单信息表
create table  if not exists customize_info(
	id int unsigned key auto_increment,
	name varchar(50) ,
	email varchar(50),
	phone varchar(11),
	submit_date varchar(20),
	planned_date varchar(20),
	type tinyint unsigned,
	url varchar(100),
	format tinyint unsigned,
	num_of_records tinyint unsigned,
	description text
);

/*更多数据表以后再建*/