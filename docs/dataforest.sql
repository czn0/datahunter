-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2017-04-29 14:12:44
-- 服务器版本： 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dataforest`
--

-- --------------------------------------------------------

--
-- 表的结构 `complaint`
--

CREATE TABLE IF NOT EXISTS `complaint` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `resource_id` bigint(20) unsigned DEFAULT NULL,
  `complaint_reason` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `customize_info`
--

CREATE TABLE IF NOT EXISTS `customize_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `submit_date` varchar(20) DEFAULT NULL,
  `planned_date` varchar(20) DEFAULT NULL,
  `type` tinyint(3) unsigned DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `format` tinyint(3) unsigned DEFAULT NULL,
  `num_of_records` tinyint(3) unsigned DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `customize_info`
--

INSERT INTO `customize_info` (`id`, `name`, `email`, `phone`, `submit_date`, `planned_date`, `type`, `url`, `format`, `num_of_records`, `description`) VALUES
(1, 'qige', 'zrq96@163.com', '13697452365', '2017-04-27', '2017-7-1', 0, 'www.baidu.com', 0, 0, '话说那个直呼类型和url是什么鬼？'),
(2, 'qige', 'zrq96@163.com', '13697452365', '2017-04-27', '2017-7-1', 0, 'www.baidu.com', 0, 0, '话说那个直呼类型和url是什么鬼？'),
(3, 'ray', 'ray@163.com', '123', '2017-04-27', '123', 0, '132', 0, 0, 'aaaaaaaaaaa'),
(4, '陈钟南', '990112156@qq.com', '111111111', '2017-04-28', '2017-5-1', 0, 'www.baidu.com', 5, 1, '爱你么么哒');

-- --------------------------------------------------------

--
-- 表的结构 `resource`
--

CREATE TABLE IF NOT EXISTS `resource` (
  `resource_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  `author` varchar(50) DEFAULT NULL,
  `dateline` int(11) DEFAULT NULL,
  `category` tinyint(3) unsigned DEFAULT NULL,
  `isfree` tinyint(3) unsigned DEFAULT NULL,
  `browsed` int(10) unsigned DEFAULT NULL,
  `liked` int(10) unsigned DEFAULT NULL,
  `download_link` varchar(300) DEFAULT NULL,
  `tag` varchar(100) NOT NULL,
  PRIMARY KEY (`resource_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- 转存表中的数据 `resource`
--

INSERT INTO `resource` (`resource_id`, `title`, `content`, `author`, `dateline`, `category`, `isfree`, `browsed`, `liked`, `download_link`, `tag`) VALUES
(1, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\3fbda77883ac93e25fec3e35583689cd.gephi_temp1475729950746', '测试'),
(2, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\933d4c5fc3ea8b8c3ca294969a88d701.gephi_temp1475729950746', '测试'),
(3, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\e31ab281fe96acfb7299a1e71825c3a5.gephi_temp1475729950746', '测试'),
(4, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\f718b75d6332f7200bd61d6fd91687ad.gephi_temp1475729950746', '测试'),
(5, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\4fe5d939e262448e301b0ce7146b8188.gephi_temp1475729950746', '测试'),
(6, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\e41a0bb4c8e95d7db05394cac0659bfc.gephi_temp1475729950746', '测试'),
(7, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\f10e9eb60c9eaacf7beac47148aa5c15.gephi_temp1475729950746', '测试'),
(8, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\b8c6cf385810330a5c864a06feb796a1.gephi_temp1475729950746', '测试'),
(9, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\0be70e4f3e71ed9638bd45815521464f.gephi_temp1475729950746', '测试'),
(10, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\e4f31f0b36b6684b3b133e3597d32c3b.gephi_temp1475729950746', '测试'),
(11, '你好', '你好', '你好', NULL, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170427\\aafb9972e8d72ce1dea1761a2c07c0af.gephi_temp1475729950746', '测试'),
(12, '11', '111', '11', 1493313600, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170428\\e632045e5f6d2658c9c7e0e54614279d.gephi_temp1475729950746', '测试2'),
(13, '11', '111', '11', 1493313685, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170428\\756ba21312f28e4101776fac59ecf873.gephi_temp1475729950746', '测试2'),
(14, '1', '1', '1', 1493313703, 1, 1, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170428\\e612e7cf79a934806431ea5d43cc03fc.gephi_temp1475729950746', '1'),
(15, '11', '11', '11', 1493313833, 11, 11, 10, 5, 'D:\\softwares\\wamp\\www\\dataforest\\public\\uploads\\resources\\20170428\\ca40a8daef229ff8301a7168577edeae.gephi_temp1475729950746', '1');

-- --------------------------------------------------------

--
-- 表的结构 `resource_tag`
--

CREATE TABLE IF NOT EXISTS `resource_tag` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `resource_id` bigint(20) unsigned DEFAULT NULL,
  `tag` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 转存表中的数据 `resource_tag`
--

INSERT INTO `resource_tag` (`id`, `resource_id`, `tag`) VALUES
(1, 11, '测试'),
(2, 12, '测试2'),
(3, 13, '测试2'),
(4, 14, '1'),
(5, 15, '1');

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `create_date` varchar(20) DEFAULT NULL,
  `level` tinyint(3) unsigned DEFAULT NULL,
  `profile_link` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
