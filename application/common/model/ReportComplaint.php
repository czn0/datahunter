<?php
namespace app\common\model;

use think\Model;

class ReportComplaint extends Model
{
	//插入一条举报记录
	public function complainReport($report_id, $complain_reason){
		return $this->insert(['report_id'=>$report_id, 'complaint_reason'=>$complain_reason]);
	}
}