<?php
namespace app\common\model;

use think\Model;

class Report extends Model
{
	//插入一条举报记录
	public function getAllReport(){
		return $this->select();
	}

	public function getAllReportPagination()
	{
		return $this->paginate();
	}

	public function getOneReport($report_id)
	{
		return $this->where('report_id', $report_id)->find();
	}

	public function getSomeReport($category)
	{
		return $this->where('category', $category)->select();
	}

	public function uploadReport($report_data)
	{
		return $this->allowField(true)->save($report_data);
	}
}