<?php
namespace app\common\model;

use think\Model;

class ResourceComplaint extends Model
{
	//插入一条举报记录
	public function complainResource($resource_id, $complain_reason){
		return $this
		->insert(['resource_id'=>$resource_id, 'complaint_reason'=>$complain_reason]);
	}
}