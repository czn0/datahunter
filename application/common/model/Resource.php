<?php
namespace app\common\model;

use think\Model;

class Resource extends Model
{
	//获取资源的信息，用于共享社区主页显示资源的标题，贡献者，浏览数和点赞数
	public function getInfo($resource_id)
	{
		return $this->where('resource_id', $resource_id)
			->field('resource_id, title, browsed, liked, author')
			->select();
	}
	////获取全部资源的信息，用于共享社区主页显示资源的标题，贡献者，浏览数和点赞数
	public function getAllInfo(){
		$all_info = $this->field('resource_id, title, browsed, liked, author')
						->select();
		return $all_info;
	}

	//获取资源的正文内容
	public function getContent($resource_id){
		$content = $this->where('resource_id', $resource_id)
			->field('resource_id,title,content,author,dateline,category,isfree,download_link')
			->select();
		return $content;
	}

	//添加一个资源
	public function add($data){
		return $this->insertGetId($data);
		// return $this->getLastInsID();
	}

	// 其实这个方法没啥用
	public function getDownloadLink($resource_id){
		return $this->where('resource_id', $resource_id)
			->field('download_link')
			->select();
	}
}

