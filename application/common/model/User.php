<?php
namespace app\common\model;

use think\Model;

class User extends Model
{
	// 插入一条用户记录，用于注册逻辑
	public function addUser($user_data){
		return $this->allowField(true)->save($user_data);
	}

	public function getUserInfoByAccessToken($accessToken)
	{
		return $this->where('access_token', $accessToken)->find();
	}

	public function getUserInfoByUsername($username)
	{
		return $this->where('username', $username)->find();
	}

	// 修改一条用户记录，用于修改个人信息
	public function updateUserInfo($user_data)
	{
		
	}

	public function updateExpireTime($username)
	{
		return $this->where('username', $username)->setField('expire_time', time());
	}

	public function activate($accessToken)
	{
		return $this->where('access_token', $accessToken)
					->setField('activated',1);
	}
}