<?php
namespace app\report\controller;
use think\Controller;
use app\api\controller\Report;
// require_once('../application/api/controller/Report.php');
class Index EXTENDS Controller
{
    public function index()
    {
    	$report_class = new Report;
    	$all_report = $report_class->getAllReportPagination();
    	$this->assign('all_report',$all_report);
        return $this->fetch();
    }

    public function complain()
    {
    	return $this->fetch('complain');
    }
}
