<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

return [
    '__pattern__' => [
        'name' => '\w+',
    ],
    '[hello]'     => [
        ':id'   => ['index/hello', ['method' => 'get'], ['id' => '\d+']],
        ':name' => ['index/hello', ['method' => 'post']],
    ],

    'community/[:page]' => [
    	'community/Index/index', ['method' => 'get'], ['page' => '\d+'],
    ],
    'resource/:resource_id' => [
    	'community/Resource/resource', ['method' => 'get'], ['resource_id' => '\d+'],
    ],
    'resource/complain' => [
        'community/Index/complain', ['method' => 'get'],
    ],
    'report/[:category]' => [
        'report/Index/index', ['method' => 'get'], ['category' => '\w+'],
    ],
     'report/complain' => [
        'report/Index/complain', ['method' => 'get'],
    ],
    'aboutus/:title' => [
    	'aboutus/Index/index', ['method' => 'get'], ['title' => '\w+'],
    ],
    'customization/:procedure' => [
    	'customization/Index/index', ['method' => 'get'], ['procedure' => '\w+'],
    ],
    'admin/[:module]' =>[
        'admin/Index/index', ['method' => 'get'], ['module' => '\w+'],
    ],

    'api/customize' => [
        'api/Customize/process', ['method' => 'post']
    ],
    'api/resource_info/[:resource_id]' => [
        'api/ResourceInfo/getResourceInfo', ['method' => 'post, get'], ['resource_id' => '\d+'],
    ],
    'api/all_resource_info' => [
        'api/ResourceInfo/getAllResourceinfo', ['metnod' => 'get'], 
    ],
    'api/resource_content/[:resource_id]' => [
        'api/ResourceContent/getResourceContent', ['method' => 'post, get'], ['resource_id' => '\d+'],
    ],
    'api/complain/report' => [
        'api/Complain/complainReport', ['method' => 'post'],
    ],
    'api/complain/resource' => [
        'api/Complain/complainResource', ['method' => 'post'],
    ],
    'api/upload' => [
        'api/Upload/upload', ['method' => 'post'],
    ],
    'api/download/[:resource_id]' => [
        'api/Download/download', ['method' => 'get'], ['resource_id' => '\d+'],
    ],
    'api/report/get_all_report' => [
        'api/Report/getAllReport', ['method' => 'get|post'],
    ],
    'api/report/get_one_report/:report_id' => [
        'api/Report/getOneReport', ['method' => 'post|get'], ['resource_id' => '\d+'],
    ],
    'api/report/get_some_report/:report_category' => [
        'api/Report/getSomeReport', ['method' => 'get|post'], ['report_category' => '\w+'],
    ],
    'api/report/upload_report' => [
        'api/Report/UploadReport', ['method' => 'post'], 
    ],

];
