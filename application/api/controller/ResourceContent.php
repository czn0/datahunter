<?php
namespace app\api\controller;
use think\Controller;
class ResourceContent extends Controller
{
	private $model;
	public function _initialize(){
		$this->model = model('Resource');
	}

	// 获取资源正文信息，返回json给前端
	public function getResourceContent(){
		$resource_id = input('resource_id');
		$resource_content = $this->model->getContent($resource_id);
		$resource_content[0]['download_link'] = 'api/download/'.$resource_id;
		// dump($resource_content);
		return $resource_content[0]->toJson();
	}
}