<?php
namespace app\api\controller;
use think\Controller;
class Report extends Controller
{
	private $report;
	// 获取数据模型
	public function _initialize(){
		$this->report = model('report');
	}
	
	// 举报逻辑，用于举报某个资源，未完成
	public function getAllReport(){
		return  json_encode($this->report->getAllReport());
	}

	public function getAllReportPagination()
	{
		return $this->report->getAllReportPagination();
	}

	public function getOneReport()
	{
		$report_id = input('report_id');
		return $this->report->getOneReport($report_id);
	}

	public function getSomeReport()
	{
		$report_category = input('report_category');
		return json_encode($this->report->getSomeReport($report_category));
	}

	public function uploadReport()
	{
		$report_data = input('post.');
		$report_data['upload_date'] = date('Y-m-d');
		$report_data['browsed'] = 0;
		
		if($this->report->uploadReport($report_data))
			$this->success('快报内容上传成功！');
		else {
			$this->error('快报内容上传失败！');
		}
	}
}