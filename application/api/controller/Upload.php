<?php
namespace app\api\controller;
use think\Controller;
class Upload extends Controller
{

	private $resource;
	private $resource_tag;
	public function _initialize(){
		$this->resource = model('Resource');
		$this->resource_tag = model('Resource_tag');
	}

	// 资源上传逻辑
	public function upload(){
		// 获取上传的文件并移到存放资源的路径
		$file = request()->file('file');
		$info = $file->move(ROOT_PATH . 'public' . DS . 'uploads/resources');
		if($info)
			$download_link = ROOT_PATH . 'public' . DS . 'uploads\\resources\\'.$info->getSaveName();
		else
			$file->getError();

		//数据入库
		$data =[
			'title' => input('title'),
			'content' => input('content'),
			'author' => input('author'),
			'category' => input('category'),
			'dateline' => time(),
			'isfree' => input('isfree'),
			'tag' => input('tag'),
			'download_link' => $download_link,
		];
		// dump($data);
		$resource_id = $this->resource->add($data);

		// 根据操作结果给出成功或失败的响应
		$this->resource_tag->add_tag(['resource_id'=>$resource_id, 'tag'=>input('tag')]);
		if($resource_id)
			$this->success('upload successfully');
		else 
			$this->error('upload failfed');
	}
}
