<?php
namespace app\api\controller;
use think\Controller;

class Download extends Controller
{
	private $resource;
	public function _initialize(){
		$this->resource = model('Resource');
	}

	// 登陆逻辑，根据资源id返回资源
	public function download(){
		$resource_id = input('resource_id');
		$download_link = $this->resource->getDownloadLink($resource_id);
		$file = $download_link[0]['download_link'];
		header('content-disposition:attachment; filename='.basename($file));
		header('content-length:'.filesize($file));
		readfile($file);
	}
}