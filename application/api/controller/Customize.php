<?php
namespace app\api\controller;
use think\Controller;
class Customize extends Controller
{
	private $model;
	// 获取数据模型
	public function _initialize(){
		$this->model = model('CustomizeInfo');
	}

	// 处理个性定制提交的订单，主要是入库
	public function process(){
		$form_data = input('post.');
		// dump($form_data);
		array_pop($form_data);
		$form_data['submit_date'] = date('Y-m-d');
		// dump(json_encode($form_data));
		$res = $this->model->add($form_data);
		if($res)
			$this->success('Succeeded');
		else
			$this->error('Failed');
	}
}