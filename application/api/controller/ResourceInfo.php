<?php
namespace app\api\controller;
use think\Controller;
class ResourceInfo extends Controller
{
	private $resource;
	public function _initialize(){
		$this->resource = model('Resource');
	}

	// 获取资源信息，返回json给前端
	public function getResourceInfo(){
		$resource_id = input('resource_id');
		$resource_info = $this->resource->getInfo($resource_id);
		return $resource_info[0]->toJson();
	}

	// 获取全部资源信息，返回json给前端
	public function getAllResourceInfo(){
		$all_resource_info = $this->resource->getAllInfo();
		return json_encode($all_resource_info, JSON_FORCE_OBJECT);
	}
	
}