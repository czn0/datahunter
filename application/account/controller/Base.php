<?php
namespace app\account\controller;
use think\Controller;

/*
实现会话机制的基础类
Login、User等类会继承这个类
然后会自动检测用户近期（24分钟内）有没有登陆过
若没有登陆，则访问User时会重定向到登陆界面，要求用户先登陆
若已经登陆过，则访问Login时会重定向到User，无需在登陆
*/
class Base EXTENDS Controller 
{
	public $username;
	public function _initialize() //一个自动调用方法
	{
		$isLogin = $this->isLogin();
		// 若用户未曾登陆，则重定向到登陆界面
		if(!$isLogin)
			return $this->redirect(url('login/index'));
	}

	// 检测用户是否已经登陆过
	public function isLogin(){
		$username = $this->getLoginStuId();
		if($username)
			return true; //若存在session信息，则证明用户曾登陆过
		return false;    //若不存在session信息，则说明用户未曾登陆
	}

	// 获取登陆登陆用户的session信息
	public function getLoginStuId(){
		if(!$this->username)
			$this->username = session('user', '', 'account'); //获取session信息
		return $this->username;
	}
}