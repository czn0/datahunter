<?php
namespace app\account\controller;
use think\Controller;
use phpmailer\Email;
class Register EXTENDS Controller
{
	public function index()
	{
		return $this->fetch(); //显示注册界面
	}

	// 处理注册的逻辑
	public function register(){
		// 获取post过来的表单信息
		$data = [
			'username' => input('username'),
			'password' => md5Process(input('password')),
			'confirm_password' => md5Process(input('confirm_password')),
		];

		$accountResult = Model('User')->get(['username'=>$data['username']]);
        if($accountResult) {
            $this->error('该用户存在，请重新选择');
        }

		if($data['password'] != $data['confirm_password'])
			$this->error('两次输入的密码不一致！');

		$data['create_date'] = date('Y-m-d H:i:s');
		$data['level'] = 1;
		$data['activated'] = 0;
		$data['access_token'] = md5Process($data['username'].''.$data['create_date']);
		$data['expire_time'] = time();
		$data['profile_link'] = 'D:\softwares\wamp\www\dataforest\public\static\img\avatar.png';
		$link = 'http://192.168.1.128/dataforest/public/account/register/activate?accessToken='.$data['access_token'];
		$res = model('User')->addUser($data);
		if($res){
			// $this->sendEmail($data['username'], $link);
			$this->send_activation_email($data['username']);
			$this->success('注册成功！');
		}
		else 
			$this->error('注册失败！');
	}

	public function sendEmail($username, $link)
	{
		$mail = new Email;
		$mail->send($username, '激活账号', $link);
	}

	public function activate()
	{
		$access_token = input('accessToken');
		$user = model('User');
		$user_info = $user->getUserInfoByAccessToken($access_token);
		$nowTime = time()-60*60*24;
		if($nowTime > $user_info['expire_time'])
			$this->error('链接已失效，请重新发送');
		$res = $user->activate($access_token);
		if($res)
			$this->success('激活成功！', 'http://192.168.1.128/dataforest/public/account/login');
		else
			$this->error('激活失败！', 'http://192.168.1.128/dataforest/public/account/register/sendActivationEmailAgain');
	}

	public function send_activation_email($username)
	{
		$user_info = model('User')->getUserInfoByUsername($username);
		$access_token = $user_info['access_token'];
		$link = 'http://192.168.1.128/dataforest/public/account/register/activate?accessToken='.$access_token;
		model('User')->updateExpireTime($username);
		$this->sendEmail($username, $link);
	}

	public function sendActivationEmailAgain()
	{
		return $this->fetch('send_activation_email');
	}
}