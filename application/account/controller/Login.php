<?php
namespace app\account\controller;
use think\Controller;
class Login EXTENDS Base
{
	private $user;
	public function _initialize(){
		$this->user = model('User');
	}

	public function index(){
		if(request()->isPost()) { //判断客户端的请求是post还是其他
            // 登录的逻辑
            //获取相关的数据
            $data = input('post.');
            // 通过post过来的用户名 获取 用户相关信息
            $ret = $this->user->get(['username'=>$data['username']]);

            if(!$ret ) {
                $this->error('改用户不存在');
            }

            if($ret->password != md5Process($data['password'])) {
                $this->error('密码不正确');
            }

            if($ret->activated != 1)
                $this->error('用户未激活');

            
            // 保存用户session信息  
            session('user', $ret, 'account');
            return $this->success('登录成功', url('user/index'));


        }else { //当客户端请求是get请求时
            
            $stu_id = session('user', '', 'account');   // 获取session
            
            if($stu_id ) {                       //用户近期曾经登陆过，直接跳转用户中心主页
                return $this->redirect(url('user/index'));
            }
            return $this->fetch();
        }
	}


	public function logout() {
        // 清除session
        session(null, 'account');
        // 跳出
        $this->redirect('login/index');
    }

}