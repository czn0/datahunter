<?php
namespace app\community\controller;
use think\Controller;
class Index extends Controller
{
    public function index()
    {
        return $this->fetch(); //显示共享社区页面
    }

    public function complain()
    {
    	return $this->fetch('complain');
    }
}
