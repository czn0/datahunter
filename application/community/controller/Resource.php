<?php
namespace app\community\controller;
use think\Controller;
class Resource extends Controller
{
	public function resource(){
		$id = input('param.id');
		$this->assign('id', $id);
		return $this->fetch(); //显示某项资源正文
	}
}